# Vanilla JS Carousel FT V1.0

This is a native JavaScript Carousel example. No libraries or frameworks used in this component.

###### Installation

1. Running a quick JSON-server

JSON has to load over the HTTP protocol rather than the local file protocol in Chrome. Otherwise it gives a cross domain error. It treats each file as a different domain so you need to run it in a web server.

The easiest and quickest way is the following:

From the terminal, run the following command to install json-server:

> sudo npm install -g json-server

Once it is installed, go to the project folder and run the following command:

> json-server --watch data.json

Make sure you have data.json in your project folder!

2. Getting the JSON data correctly

Once the server is up and running you'll see an output like this:

---

\{^\_^}/ hi!

Loading data.json
Done

Resources
http://localhost:3000/carousel

Home
http://localhost:3000

Type s + enter at any time to create a snapshot of the database
Watching...

---

In the script.js, make sure your JSON_URL global variable is http://localhost:3000/carousel.

3. Open the HTML in browser and have fun!

Finally after steps 1 and 2 just open the index.html in your browser! (Preferably Chrome)
